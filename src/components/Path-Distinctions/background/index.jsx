import React from "react";

import Pathdistbackgroundsvg from "./path-distinctionsbackground.svg";
import "./background.css"

class Pathdistbackground extends React.Component {
  render() {
    return (
      <div className="PathdistBack-container">
        <img src={Pathdistbackgroundsvg} alt="background" draggable="false"/>
        <h1 className="PathdistBack-title">Path and Distinctions</h1>
      </div>
    );
  }
}
export default Pathdistbackground;