import React from "react";

import "./data.css"
import Pathdistbackground from "../background/index";

class Pathdist extends React.Component {
    constructor() {
        super();
        this.state = {
            Path : {
                name : "Path of The Endless Sword : D10",
                SFX  : [["Flowing Sword:", "Enforces a sword with madra and aura. This technique grows stronger the longer it is held.", "Split Endless Sword power dice into 2d8 or 3d6."],
                        ["Rippling Sword:", "Sends razor sharp slashes through the air. Its appearance has been described as a ripple of razor-thing glass. ","Attack at a distance Path decrement dice."],
                        ["The Endless Sword:", "This technique causes the sword aura around the user to resonate in accordance with the user's madra.","Attack anyone with edged weapons and Step up the effect die by one."],
                    ]
            },
            
            SFXhtml : "",
            Disthtml : "",
        };
    }

    componentDidMount(){
        this.setDataSFX();
    }

    setDataSFX(){
          var data = this.state.Path.SFX;
          var loopData = '';
          var i ;
          for(i=0; i < data.length; i++){
              loopData += `<div class="Pathdist-data-sfxname">${data[i][0]}</div>`;
              loopData += `<div class="Pathdist-data-sfxdetails">${data[i][1]}</div>`;
              loopData += `<div class="Pathdist-data-sfxeffect">${data[i][2]}</div>`;
              
          }
          this.setState({SFXhtml: loopData});
    }

    render() {
        // To do: 
        // https://stackoverflow.com/questions/29044518/safe-alternative-to-dangerouslysetinnerhtml
        // const sanitizer = dompurify.sanitize;
        // dangerouslySetInnerHTML={{__html: sanitizer(title)}};
        return (
        <div className="Pathdist-container">
            <Pathdistbackground/>

            <div className="Pathdist-data">
                <div className="Pathdist-data-name">{this.state.Path.name}</div>
                <div className="Pathdist-data-sfx" 
                    dangerouslySetInnerHTML={{__html: this.state.SFXhtml}}>
                </div>
            </div>


        </div>
        );
    }
}
export default Pathdist;