import React from "react";

import "./data.css"
import Detailsbackground from "../background/index";

class DetailsTitle extends React.Component {
    constructor() {
        super();
        this.state = {
            cname : "Lindon",
            clan  : "Auralius",
            path  : "Twin Stars",
            stage : "High Gold",
            auth  : "N/A",
        };
    }
    
    render() {
        return (
        <div className="DetailsTitle-container">
            <Detailsbackground/>

            <div className="Detailsdata-container">
                <span className="Detailsdata-titletext Detailsdata-Cname">
                    Name: <span className="Detailsdata-datatext">{this.state.cname}</span></span>
                <span className="Detailsdata-titletext Detailsdata-Clan">
                    Clan/Sect: <span className="Detailsdata-datatext">{this.state.clan}</span></span>

                <div className="Detailsdata-titletext Detailsdata-Path">
                    Path: <span className="Detailsdata-datatext">{this.state.path}</span></div>
                <div className="Detailsdata-titletext Detailsdata-Stage">
                    Stage: <span className="Detailsdata-datatext">{this.state.stage}</span></div>

                <div className="Detailsdata-titletext Detailsdata-Auth">
                    Authority: <span className="Detailsdata-datatext">{this.state.auth}</span></div>
            </div>

        </div>
        );
    }
}
export default DetailsTitle;