import React from "react";

import detailsbackgroundsvg from "./details-background.svg";
import cradleiconpng from "./cradle-icon.png";
import bordericonpng from "./border-icon.svg";

import "./background.css"

class detailsbackground extends React.Component {
  render() {
    return (
      <div className="detailsback-container">
        <img src={detailsbackgroundsvg} alt="background" draggable="false"/>
        <img src={cradleiconpng} alt="background" draggable="false" 
          className="detailsback-cradleicon"/>
        <img src={bordericonpng} alt="background" draggable="false" 
          className="detailsback-bordericon"/>

      </div>
    );
  }
}
export default detailsbackground;