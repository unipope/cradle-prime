import React from "react";

import Traitsbackgroundsvg from "./traits-background.svg";
import "./background.css"

class Traitsbackground extends React.Component {
  render() {
    return (
      <div className="TraitsBack-container">
        <img src={Traitsbackgroundsvg} alt="background" draggable="false"/>
        <h1 className="AbilitiesBack-title-As">Ability scores</h1>
        <h1 className="AbilitiesBack-title-DS">Distinctions & Sig Assets</h1>
      </div>
    );
  }
}
export default Traitsbackground;