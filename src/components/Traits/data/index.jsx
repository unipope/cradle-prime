import React from "react";

import "./data.css"
import Traitsbackground from "../background/index";

class TraitsData extends React.Component {
    constructor() {
        super();
        this.state = {
            str : "D6",
            dex : "D6",
            men : "D12",
            soc : "D8",

            Dist : [
                ["Copper Eyes", "Ability to perceive aura."],
                ["Iron body", "(Bloodforged) Use pp to decrement doom pool by one."],
                ["Jade senses", "Increment Mental by one."],
                ["Gold sign", "Black or Sapphire eyes."],
                ["Soul fire", "Roll one dice with advantage for a pp. Try and control other aura sources."],
            ],

            SAsset : [
                ["Wavedancer", "A flying sword with water/wind aspects. Add a D8"],
                ["Archstone", "Use a PP to decrement enemy powerset and increment your own after successful attack."]
            ],

            SaDisthtml : "",
        };

        this.dumpSaDist = this.dumpSaDist.bind(this); 
    }

    componentDidMount(){
        this.dumpSaDist();
    }

    dumpSaDist(){
        let Ddata = this.state.Dist;
        let Sadata = this.state.SAsset;
        let loopData ="";

        // add Distinction data
        if (Ddata.length !== 0) {
            loopData = '<div class="Pathdist-data-name">Distinctions:</div>';
        }
        for (const [,val] of Object.entries(Ddata)) {
            loopData += `<div class="Traitsdata-ability-signame">${val[0]} : 
            <span class="Traitsdata-ability-sigdetails">${val[1]}</span></div>`;
        }
        
        // add Signature asset data
        if (Sadata.length !== 0) {
            loopData += `<div class="Pathdist-data-name">Signature assets:</div>`;
        }
        for (const [,val] of Object.entries(Sadata)) {
            loopData += `<div class="Traitsdata-ability-signame">${val[0]} : 
            <span class="Traitsdata-ability-sigdetails">${val[1]}</span></div>`;
        }

        this.setState({SaDisthtml:loopData});
    }

    render() {
        return (
        <div className="Traitsdata-container">
            <Traitsbackground/>

            
            <div className="Traitsdata-ability-title">
                <div className="Traitsdata-str-title">Strength</div>
                <div className="Traitsdata-dex-title">Dexterity</div>
                <div className="Traitsdata-men-title">Mental</div>
                <div className="Traitsdata-soc-title">Social</div>
            </div> 

            <div className="Traitsdata-ability-data Traitsdata-str-data">{this.state.str}</div>
            <div className="Traitsdata-ability-data Traitsdata-dex-data">{this.state.dex}</div>
            <div className="Traitsdata-ability-data Traitsdata-men-data">{this.state.men}</div>
            <div className="Traitsdata-ability-data Traitsdata-soc-data">{this.state.soc}</div>

            <div className="Traitsdata-ability-SaDist" 
                dangerouslySetInnerHTML={{__html: this.state.SaDisthtml}}>
            </div>

        </div>
        );
    }
}
export default TraitsData;