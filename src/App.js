import './App.css';

import Details  from "./components/Details/data";
import Traits   from "./components/Traits/data";
import Pathdist from "./components/Path-Distinctions/data";
import Trackers from "./components/Trackers/data";

function App() {
  return (
    <div className="App">
        <Details />

        <Traits />
        <Pathdist />

        <Trackers />
    </div>
  );
}
export default App;
