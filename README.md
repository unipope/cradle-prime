# Cortex Prime character sheet for Cradle prime

## Tech used :
React<br />
Figma (Creating SVGs)<br />

## Create a character :
1. Assign d10, d8, d6, d6, d4 between your Attributes.
1. You have 8d8 to spend on your Path, Signature assets and iron body.
1. Power Sets (Path), Signature assets and iron body:
    1. A d8 can be spent to…
        * Add two d6 Powers.
        * Add a d8 Power.
        * Add an Expert Specialty.
        * Add an SFX to a Power Set.
        
    1. 2d8 can be spent to…
        * Add a d10 Power.
        * Add a signature asset i.e. legendary arms or armor and/or other items.
        * You may add one additional Limit to a Power Set to get an additional d8.
        
    1. Example: Path of The Endless Sword : d10
        * SFX: Flowing Sword (Enforcer): Enforces a sword with madra and aura. This technique grows stronger the longer it is held. Split Endless Sword power dice into 2d8 or 3d6.
        * SFX: Rippling Sword (Striker): Sends razor sharp slashes through the air. Its appearance has been described as a ripple of razor-thing glass. Attack at a distance.
        * SFX: The Endless Sword (Ruler): This technique causes the sword aura around the user to resonate in accordance with the user's madra. Attack anyone with edged weapons and Step up the effect die by one.

3. Distinctions:
    * Upgraded or Perfect / Advanced iron body.
    * Advancements:
        * Copper Eyes: ability to perceive aura.
        * Iron body: Each iron body makes the user more effective at using techniques. But specific versions can make the practitioner better in other ways.
            * Example: 
                * Bloodforged Iron body - Use pp to decrement doom pool by one.
                * Steelforged Iron body - Increments strength dice by one.
        * Jade senses: Increment mental by one 
        * Gold sign: Stylish things
        * Soul fire: Roll one dice with advantage for a pp.

4. Signature Assets:
    * The unique gear and equipment a Scrated artist posesses.
    * Examples:
        * Ancestors Spear d8 - Use a pp to decrement enemy power set dice and increment your own after successful attack.
        * Yerin's Blade d6 - In the lord realm can use binding to increase the effect die of The Endless Sword (Ruler) to a d12 but will cripple you for the rest of the scene. 
        * Wavedancer d6 - 
        * Archstone d6 - Use a pp to decrement enemy power set dice and increment your own after successful attack.
